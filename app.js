require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});

// Config environment
const express = require("express"); // Import express
const fs = require("fs");
const path = require("path");
const mongoSanitize = require("express-mongo-sanitize");
const xss = require("xss-clean");
const rateLimit = require("express-rate-limit");
const hpp = require("hpp");
const helmet = require("helmet");
const cors = require("cors");
const morgan = require("morgan");

const app = express(); // Make express app
const mongoose = require("mongoose");
const Item = require("./models/Item");
const History = require("./models/History");
const validator = require("validator");

// Database config
mongoose
  .connect(process.env.MONGO_URI)
  .then(() => console.log("MongoDB Connected"))
  /* istanbul ignore next */
  .catch((err) => console.log(err));

// CORS
app.use(cors());

// Sanitize data
app.use(mongoSanitize());

// Prevent XSS attact
app.use(xss());

// Rate limiting
const limiter = rateLimit({
  windowMs: 1000,
  max: 25,
});

app.use(limiter);

// Prevent http param pollution
app.use(hpp());

// Use helmet
app.use(
  helmet({
    contentSecurityPolicy: false,
  })
);

/* Use error handler */
const errorHandler = (err, req, res, next) => {
  return res.status(err.statusCode || 500).json({
    errors: err.messages || [err.message],
  });
};

/* istanbul ignore next */
if (process.env.NODE_ENV === "development" || process.env.NODE_ENV === "test") {
  app.use(morgan("dev"));
} else {
  // create a write stream (in append mode)
  let accessLogStream = fs.createWriteStream(
    path.join(__dirname, "access.log"),
    {
      flags: "a",
    }
  );

  // setup the logger
  app.use(morgan("combined", { stream: accessLogStream }));
}

/* Enables req.body */
app.use(express.json()); // Enables req.body (JSON)
// Enables req.body (url-encoded)
app.use(
  express.urlencoded({
    extended: true,
  })
);

app.get("/", (req, res) => {
  res.send("Hello, from GA Assessment Project BE");
});

app.get(
  "/items",
  async (req, res, next) => {
    const pageSize = parseInt(req.query.limit) || 15;
    const currentPage = req.query.page || 1;

    const data = await Item.find()
      .skip(pageSize * (currentPage - 1))
      .limit(pageSize)
      .sort("-createdAt");

    if (data.length) {
      return res.status(200).json({ data });
    } else {
      return next({ statusCode: 404, message: "Items not found." });
    }
  },
  errorHandler
);

app.get(
  "/logs",
  async (req, res, next) => {
    const pageSize = parseInt(req.query.limit) || 15;
    const currentPage = req.query.page || 1;

    const data = await History.find()
      .skip(pageSize * (currentPage - 1))
      .limit(pageSize)
      .sort("-createdAt");

    if (data.length) {
      return res.status(200).json({ data });
    } else {
      return next({ statusCode: 404, message: "Item histories not found." });
    }
  },
  errorHandler
);

app.get(
  "/items/:id",
  async (req, res, next) => {
    if (!validator.isMongoId(req.params.id)) {
      return next({ message: "Id is not valid", statusCode: 400 });
    }

    const data = await Item.findById(req.params.id);
    if (data) {
      return res.status(200).json(data);
    } else {
      return next({ statusCode: 404, message: "Item not found." });
    }
  },
  errorHandler
);

app.post(
  "/items",
  async (req, res, next) => {
    try {
      // Check req.body input
      const errorMessages = [];
      if (
        !req.body.name ||
        !req.body.stock ||
        !req.body.price ||
        !req.body.category
      ) {
        errorMessages.push("Missing required fields.");
      }
      if (req.body.price <= 0) {
        errorMessages.push("Price is not valid.");
      }
      if (req.body.stock < 0) {
        errorMessages.push("Stock is not valid.");
      }
      if (errorMessages.length > 0) {
        return next({ messages: errorMessages, statusCode: 400 });
      }

      await Item.create(req.body);
      return res.status(201).json({ message: "Item successfully created." });
    } catch (error) {
      return next(error);
    }
  },
  errorHandler
);

app.get(
  "/items/:id/log",
  async (req, res, next) => {
    if (!validator.isMongoId(req.params.id)) {
      return next({ message: "Id is not valid", statusCode: 400 });
    }

    const pageSize = parseInt(req.query.limit) || 15;
    const currentPage = req.query.page || 1;

    const data = await History.find({ itemId: req.params.id })
      .skip(pageSize * (currentPage - 1))
      .limit(pageSize)
      .sort("-createdAt");

    if (!data.length)
      return next({ statusCode: 404, message: "Item history not found." });

    return res.status(200).json({ data });
  },
  errorHandler
);

app.put(
  "/items/:id/log",
  async (req, res, next) => {
    if (!validator.isMongoId(req.params.id)) {
      return next({ message: "Id is not valid", statusCode: 400 });
    }
    if (req.body.quantity < 0) {
      return next({ message: "Qty is not valid.", statusCode: 400 });
    }

    /* istanbul ignore next */
    const types = ["stock in", "stock out"];
    if (!types.includes(req.body.type)) {
      return next({
        message: "Type is not valid. Input stock in/ stock out.",
        statusCode: 400,
      });
    }

    const data = await Item.findById(req.params.id);

    if (!data) return next({ statusCode: 404, message: "Item not found." });

    if (req.body.type === "stock out" && +data.stock < +req.body.quantity) {
      return next({
        statusCode: 400,
        message: "Not enough stock to take out.",
      });
    }

    let newStock;
    req.body.type === "stock out"
      ? (newStock = +data.stock - +req.body.quantity)
      : (newStock = +data.stock + +req.body.quantity);

    await Item.findOneAndUpdate(
      { _id: req.params.id },
      { stock: newStock },
      { new: true }
    );

    await History.create({
      itemId: req.params.id,
      transactionType: req.body.type,
      quantity: +req.body.quantity,
      updatedStockValue: newStock,
    });

    return res
      .status(200)
      .json({ message: "Item stock successfully updated." });
  },
  errorHandler
);

app.put(
  "/items/:id",
  async (req, res, next) => {
    if (!validator.isMongoId(req.params.id)) {
      return next({ message: "Id is not valid", statusCode: 400 });
    }
    const errorMessages = [];
    if (req.body.price && req.body.price <= 0) {
      errorMessages.push("Price is not valid.");
    }
    if (req.body.stock && req.body.stock < 0) {
      errorMessages.push("Stock is not valid.");
    }
    if (errorMessages.length > 0) {
      return next({ messages: errorMessages, statusCode: 400 });
    }
    const data = await Item.findOneAndUpdate({ _id: req.params.id }, req.body, {
      new: true,
    });
    if (!data) return next({ statusCode: 404, message: "Item not found." });

    return res.status(201).json({ message: "Item successfully updated." });
  },
  errorHandler
);

app.delete(
  "/items/:id",
  async (req, res, next) => {
    const data = await Item.findOneAndDelete({ _id: req.params.id });
    if (data) {
      return res.status(200).json({ message: "Item successfully deleted." });
    } else {
      return next({ statusCode: 404, message: "Item not found." });
    }
  },
  errorHandler
);

/* If route not found */
app.all(
  "*",
  (req, res, next) => {
    try {
      /* istanbul ignore next */
      next({ message: "Endpoint not found", statusCode: 404 });
    } catch (error) {
      /* istanbul ignore next */
      next(error);
    }
  },
  errorHandler
);

/* Run the server */
/* istanbul ignore next */
const port = process.env.PORT || 3000;
if (process.env.NODE_ENV !== "test") {
  app.listen(port, () => console.log(`Server running on ${port}`));
}

// Export app for testing
module.exports = app;
