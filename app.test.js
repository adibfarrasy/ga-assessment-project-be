// Adib's Code
const request = require("supertest");
const app = require("./app");
const mongoose = require("mongoose");
const Item = require("./models/Item");
const History = require("./models/History");
const faker = require("faker");

let itemId = "";

beforeAll(async () => {
  const itemTest = await Item.create({
    name: "Test Item",
    stock: 5,
    price: 10000,
    category: "gadget",
  });

  itemId = itemTest._id.toString();
});

afterAll(async () => {
  await Item.deleteMany();
  await History.deleteMany();
  mongoose.disconnect();
});

describe("Pulse Check", () => {
  it("Success", async () => {
    const response = await request(app).get(`/`);

    expect(response.statusCode).toEqual(200);
  });
});

describe("Get Items", () => {
  it("Success", async () => {
    const response = await request(app)
      .get(`/items`)
      .query({ limit: 3, page: 1 });

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("data");
  });
});

describe("Get One Item", () => {
  it("Success", async () => {
    const response = await request(app).get(`/items/${itemId}`);
    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
  });
  it("ID Not Valid", async () => {
    const response = await request(app).get(`/items/${itemId}0`);
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
  it("Item Not Found", async () => {
    const response = await request(app).get(`/items/${0 + itemId.slice(1)}`);
    expect(response.statusCode).toEqual(404);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});

describe("Add Item", () => {
  it("Success", async () => {
    const response = await request(app).post(`/items`).send({
      name: "Test Item 2",
      stock: 1,
      price: 50000,
      category: "kitchenware",
    });
    expect(response.statusCode).toEqual(201);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("message");
  });
  it("Invalid Item Category", async () => {
    const response = await request(app).post(`/items`).send({
      name: "Test Item 3",
      stock: 1,
      price: 50000,
      category: "monkeywrench",
    });
    expect(response.statusCode).toEqual(500);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
  it("Missing Required Field(s)", async () => {
    const response = await request(app).post(`/items`).send({
      stock: 1,
      price: 50000,
      category: "kitchenware",
    });
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
  it("Invalid Stock Value", async () => {
    const response = await request(app).post(`/items`).send({
      name: "Test Item 4",
      stock: -10,
      price: 50000,
      category: "kitchenware",
    });
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
  it("Invalid Stock Value", async () => {
    const response = await request(app).post(`/items`).send({
      name: "Test Item 5",
      stock: 100,
      price: -10,
      category: "kitchenware",
    });
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});

describe("Update Item", () => {
  it("Success", async () => {
    const response = await request(app).put(`/items/${itemId}`).send({
      name: "Test Item 6",
      stock: 1,
      price: 50000,
      category: "stationery",
    });
    expect(response.statusCode).toEqual(201);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("message");
  });
  it("ID Not Valid", async () => {
    const response = await request(app).put(`/items/${itemId}0`).send({
      name: "Test Item 7",
      stock: 1,
      price: 50000,
      category: "gadget",
    });
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
  it("Invalid Stock Value", async () => {
    const response = await request(app).put(`/items/${itemId}`).send({
      stock: -1,
      price: 50000,
      category: "kitchenware",
    });
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
  it("Item Not Found", async () => {
    const response = await request(app)
      .put(`/items/${0 + itemId.slice(1)}`)
      .send({
        stock: 10,
        price: 50000,
        category: "kitchenware",
      });

    expect(response.statusCode).toEqual(404);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
  it("Invalid Stock Value", async () => {
    const response = await request(app).put(`/items/${itemId}`).send({
      stock: 100,
      price: -10,
      category: "kitchenware",
    });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});

describe("Update Stock", () => {
  it("Success", async () => {
    const response = await request(app).put(`/items/${itemId}/log`).send({
      type: "stock in",
      quantity: 10,
    });
    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("message");
  });
  it("ID Not Valid", async () => {
    const response = await request(app).put(`/items/${itemId}0/log`).send({
      type: "stock in",
      quantity: 10,
    });
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
  it("Invalid Quantity", async () => {
    const response = await request(app).put(`/items/${itemId}/log`).send({
      type: "stock in",
      quantity: -10,
    });
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
  it("Item Not Found", async () => {
    const response = await request(app)
      .put(`/items/${0 + itemId.slice(1)}/log`)
      .send({
        type: "stock in",
        quantity: 10,
      });

    expect(response.statusCode).toEqual(404);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
  it("Not Enough Stock", async () => {
    const response = await request(app).put(`/items/${itemId}/log`).send({
      type: "stock out",
      quantity: 10000,
    });

    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("errors");
  });
});

describe("Get All Stock In/ Stock Out", () => {
  it("Success", async () => {
    const response = await request(app)
      .get(`/logs`)
      .query({ limit: 3, page: 1 });

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("data");
  });
});

describe("Get Item Stock In/ Stock Out", () => {
  it("Success", async () => {
    const response = await request(app)
      .get(`/items/${itemId}/log`)
      .query({ limit: 3, page: 1 });

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("data");
  });
});

describe("Delete Item", () => {
  it("Success", async () => {
    const response = await request(app).delete(`/items/${itemId}`);

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
    expect(response.body).toHaveProperty("message");
  });
});
