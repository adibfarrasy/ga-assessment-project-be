const mongoose = require("mongoose");

const historySchema = new mongoose.Schema(
  {
    itemId: {
      type: mongoose.Schema.Types.Mixed,
      required: true,
    },
    transactionType: {
      type: String,
      required: true,
      enum: ["stock in", "stock out"],
    },
    quantity: {
      type: Number,
      required: true,
    },
    updatedStockValue: {
      type: Number,
      required: true,
    },
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
    toJSON: { getters: true },
  }
);

module.exports = mongoose.model("History", historySchema);
